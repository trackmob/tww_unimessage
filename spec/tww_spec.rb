require 'spec_helper'

describe Tww do

  describe '.configure' do
    before do
      Tww.configure do |config|
        config.username = 'FakeUsername'
      end
    end

    it { expect(Tww.configuration.username).to eq('FakeUsername') }
  end
end
