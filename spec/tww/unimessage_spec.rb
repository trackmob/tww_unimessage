require 'spec_helper'
require "savon/mock/spec_helper"

module Tww
  describe Unimessage do
    include Savon::SpecHelper

    before(:all) { savon.mock!   }
    after(:all)  { savon.unmock! }

    before do
      Tww.configure do |config|
        config.username = 'FakeUsername'
        config.password = 'FakePassword'
      end
    end

    describe '.send' do
      context "valid params" do
        let(:to){ '554112345678' }
        let(:message){ 'Coolest message ever.' }
        let(:from){ '5511234567890' }

        it "does send sms message" do
          fixture = File.read('spec/fixtures/client/send_sms.xml')
          expected_message = {
            'NumUsu' => 'FakeUsername',
            'Senha' => 'FakePassword',
            'SeuNum' => from,
            'Celular' => to,
            'Mensagem' => message
          }

          savon.expects(:envia_sms).with(message: expected_message).returns(fixture)
          Tww::Unimessage.send(to: to, message: message, from: from)
        end
      end
    end
  end
end
