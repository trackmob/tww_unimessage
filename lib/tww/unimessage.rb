module Tww
  module Unimessage
    WSDL_URL = 'http://webservices.twwwireless.com.br/reluzcap/wsreluzcap.asmx?WSDL'
    def self.send(params)
      client = Savon.client(wsdl: WSDL_URL)
      message = {
        'NumUsu' => Tww.configuration.username, 'Senha' => Tww.configuration.password,
        'SeuNum' => params[:from], 'Celular' => params[:to], 'Mensagem' => params[:message]
      }
      response = client.call(:envia_sms, message: message)
      response.body[:envia_sms_response][:envia_sms_result] == 'OK'
    end
  end
end
